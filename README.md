# Basic Flask App Starting Point

This is a very good starting point for a Flask Application.

It has templating established and basic user registration and authentication set up.

Some of the database schema is specific to another app but it can easily be replaced.

Nga mihi,
Eddie     :)

